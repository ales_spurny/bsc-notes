var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var ngAnnotate = require('gulp-ng-annotate');
var minifyHTML = require('gulp-minify-html');
var templateCache = require('gulp-angular-templatecache');
var gls = require('gulp-live-server');


gulp.task('scripts', function() {
// Minify and copy all JavaScript (except vendor scripts)

	gulp.src('angular_modules/**/src/templates/*.html')
		.pipe(minifyHTML({
			empty: true,
			spare: true,
			quotes: true
		}))
		.pipe(templateCache())
		.pipe(gulp.dest('build/js'));

	gulp.src(['angular_modules/**/*.js', 'build/js/templates.js'])
		.pipe(concat("main.js"))
		//.pipe(ngAnnotate())
		//.pipe(uglify())
		.pipe(gulp.dest('build/js'));

	// Copy vendor files
	gulp.src([
		'bower_components/jquery/dist/jquery.min.js',
		'bower_components/bootstrap/dist/js/bootstrap.min.js',
		'bower_components/angular/angular.js',
		'bower_components/angular-ui-router/release/angular-ui-router.js',
		'bower_components/angular-translate/angular-translate.js',
		'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
		'bower_components/angular-bindonce/bindonce.js'
		])
		.pipe(concat("lib.js"))
		.pipe(gulp.dest('build/js'));

	gulp.src([
		'bower_components/bootstrap/dist/css/bootstrap.min.css',
		'bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
		'bower_components/animate.css/animate.min.css',
		'bower_components/components-font-awesome/css/font-awesome.min.css'
		])
		.pipe(concat("lib.css"))
		.pipe(gulp.dest('build/css/lib'));
});

gulp.task('lang', function () {
	gulp.src('lang/*.json')
		.pipe(gulp.dest('build/lang'));
});

gulp.task('less', function () {
	gulp.src('layout/less/main.less')
		.pipe(less())
		.pipe(gulp.dest('build/css'));
});

// Copy all static assets
gulp.task('copy', function() {
	gulp.src('layout/img/**')
		.pipe(gulp.dest('build/img'));
		
	gulp.src('bower_components/components-font-awesome/fonts/**')
		.pipe(gulp.dest('build/css/fonts'));

	gulp.src('layout/index.html')
		.pipe(gulp.dest('build'));
});

// Run server
gulp.task('default', function() {
	gulp.start('scripts', 'less', 'copy', 'lang');
	
	var server = gls.static('build', 9000);
	server.start();
	
	gulp.watch(['angular_modules/**'], ['scripts']);
	gulp.watch(['layout/less/**'], ['less']);
	gulp.watch(['layout/**/*.html'], ['copy', 'scripts']);
	gulp.watch(['layout/img/**'], ['copy']);
	gulp.watch(['lang/*.json'], ['lang']);
});