/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Notes')
	.factory('App.Notes.NotesService', NotesService);

NotesService.$inject = [
	'$http',
	'$q',
	'URL_REQUESTS'
];

/**
 * @ngdoc service
 * @name NotesService
 * @module App.Notes
 * @param $http {service} Service to communication with the remote HTTP servers
 * @param $q {service} A service that helps you run functions asynchronously
 * @param URL_REQUESTS {constant} Constant for list of URL's
 * @description
 * Service that provides data from apiary
 * @constructor NotesService
 */
function NotesService($http, $q, URL_REQUESTS) {
	return {
		notesList: angular.bind(undefined, noteOperation, 'LIST'),
		noteCreate: angular.bind(undefined, noteOperation, 'CREATE'),
		noteDelete: angular.bind(undefined, noteOperation, 'DELETE'),
		noteUpdate: angular.bind(undefined, noteOperation, 'UPDATE'),
		noteDetail: angular.bind(undefined, noteOperation, 'DETAIL')
	};
	
	function noteOperation(operation, noteId, noteTitle) {
		var httpRequest = null,
			defer = $q.defer();
		
		switch (operation) {
			case 'LIST':
				httpRequest = $http.get(URL_REQUESTS.LIST);
			break;
			case 'CREATE':
				httpRequest = $http.post(URL_REQUESTS.LIST);
			break;			
			case 'DELETE':
				httpRequest = $http.delete(URL_REQUESTS.DELETE + noteId);
			break;
			case 'UPDATE':
				var params = {title: noteTitle};
				httpRequest = $http.put(URL_REQUESTS.UPDATE + noteId, params);
			break;
			case 'DETAIL': 
				httpRequest = $http.get(URL_REQUESTS.DETAIL + noteId);
			break;
		}
		
		httpRequest.then(function(response) {
			console.log("Zahajuji operaci " + operation);
			console.log(response);
			defer.resolve(response);
		}, function(error) {
			defer.reject(error);
		});
		
		return defer.promise;
	}
}