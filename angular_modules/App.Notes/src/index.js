/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

angular
	.module('App.Notes', [])
		.constant('URL_REQUESTS', {
			'LIST': 'http://private-anon-27e1677b0-note10.apiary-mock.com/notes',
			'DELETE': 'http://private-anon-27e1677b0-note10.apiary-mock.com/notes/',
			'UPDATE': 'http://private-anon-27e1677b0-note10.apiary-mock.com/notes/',
			'DETAIL': 'http://private-anon-27e1677b0-note10.apiary-mock.com/notes/'
		})
		.config(configureStates);

configureStates.$inject = ['$stateProvider', '$urlRouterProvider'];

function configureStates($stateProvider, $urlRouterProvider) {
	
	// Default router
	$urlRouterProvider.otherwise('/notes');
	
	$stateProvider
		// Default
		.state('notes', {
			url: '/notes',
			templateUrl: 'App.Notes/src/templates/notes.html',
			controller: 'App.Notes.NotesListController',
			controllerAs: 'ntsListController',
		})
		// Router for notes list
		.state('notes.create', {
			url: '/create',
			controller: 'App.Notes.NotesCreateController',
			controllerAs: 'ntsCreateController',
			templateUrl: 'App.Notes/src/templates/notes.create.html'
		})
		// Router for notes edit
		.state('notes.edit', {
			url: '/edit/{noteId:int}',
			controller: 'App.Notes.NotesEditController',
			controllerAs: 'ntsEditController',
			templateUrl: 'App.Notes/src/templates/notes.edit.html'
		})
		// Router for notes detail
		.state('notes.detail', {
			url: '/detail/{noteId:int}',
			controller: 'App.Notes.NotesDetailController',
			controllerAs: 'ntsDetailController',
			templateUrl: 'App.Notes/src/templates/notes.detail.html'
		});
}