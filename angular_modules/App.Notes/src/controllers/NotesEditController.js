/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Notes')
	.controller('App.Notes.NotesEditController', NotesEditController);
	
NotesEditController.$inject = [
	'$stateParams',
	'$window',
	'App.Notes.NotesService'
];

/**
 * @name NotesEditController
 * @module App.Notes
 * @constructor NotesEditController
 * @param $stateParams {object} Angular ui-router stateParams object
 * @param $window {object} Window object
 * @param notesService {object} Notes servis for communication with apiary
 */
function NotesEditController($stateParams, $window, notesService) {
	var vm = this;
	
	vm.editNote = editNote;
	
	vm.noteId = null;
	vm.noteTitle = "";
	
	_init();
	
	function _init() {
		var paramNoteId = $stateParams.noteId;
		_loadDataForNote(paramNoteId);
	}
	
	function _loadDataForNote(noteId) {
		var promise;
		promise = notesService.noteDetail(noteId);

		promise.then(function(response) {
			if (response.status === 200) {
				var data = response.data;
				vm.noteId = data.id;
				vm.noteTitle = data.title;
				console.log("Edituji poznámku s ID: " + data.id);
			}
		});
		promise.catch(function() {
			console.log("Nepodařilo se získat detail poznámky");
		});
	}
	
	function editNote(noteId) {
		var promise;
		promise = notesService.noteUpdate(noteId, vm.noteTitle);
		
		promise.then(function(response) {
			if (response.status === 201) {
				var data = response.data;
				vm.noteId = data.id;
				vm.noteTitle = data.title;
				console.log("Editace poznámky s ID: " + data.id + " proběhla v pořádku");
				// Does reload after update to refresh list of notes
				//$window.location.reload();
			}
		});
		promise.catch(function() {
			console.log("Editace poznámky se nezdařila");
		});
	}
}