/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Notes')
	.controller('App.Notes.NotesListController', NotesListController);
	
NotesListController.$inject = [
	'App.Notes.NotesService'
];

/**
 * @name NotesListController
 * @module App.Notes
 * @constructor NotesListController
 * @param notesService {object} Notes servis for communication with apiary
 */
function NotesListController(notesService) {
	var vm = this;
	
	vm.deleteNote = deleteNote;
	
	vm.notes = {};
	
	_init();
	
	function _init() {
		_notesList();
	}
	
	/**
	 * Get a list of notes
	 */
	function _notesList() {
		var promise;
		
		promise = notesService.notesList();
		promise.then(function(response) {
			vm.notes = response.data;	
		});
		promise.catch(function() {
			vm.notes = {};
		});
	}
	
	/**
	 * Detele the note from a list
	 * @param int Note ID
	 */
	function deleteNote(noteId) {
		var promise;

		promise = notesService.noteDelete(noteId);

		promise.then(function(response) {
			if (response.status === 204) {
				console.log("Poznámka byla smazána");
			}
		});
		promise.catch(function() {
			console.log("Poznámka se nesmazala");
		});
	}
}