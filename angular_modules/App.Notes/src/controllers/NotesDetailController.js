/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Notes')
	.controller('App.Notes.NotesDetailController', NotesDetailController);
	
NotesDetailController.$inject = [
	'$stateParams',
	'App.Notes.NotesService'
];

/**
 * @name NotesDetailController
 * @module App.Notes
 * @constructor NotesDetailController
 * @param $stateParams {object} Angular ui-router stateParams object
 * @param notesService {object} Notes servis for communication with apiary
 */
function NotesDetailController($stateParams, notesService) {
	var vm = this;
	
	vm.noteId = null;
	vm.noteTitle = "";
	
	_init();
	
	function _init() {
		var paramNoteId = $stateParams.noteId;
		_detailNote(paramNoteId);
	}
	
	function _detailNote(noteId) {
		var promise;
		promise = notesService.noteDetail(noteId);

		promise.then(function(response) {
			if (response.status === 200) {
				var data = response.data;
				vm.noteId = data.id;
				vm.noteTitle = data.title;
				console.log("Zobrazuji detail poznámky s ID: " + data.id);
			}
		});
		promise.catch(function() {
			console.log("Nepodařilo se získat detail poznámky");
		});
	}
}