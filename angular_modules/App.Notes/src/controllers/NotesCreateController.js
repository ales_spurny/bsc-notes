/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Notes')
	.controller('App.Notes.NotesCreateController', NotesCreateController);
	
NotesCreateController.$inject = [
	'$window',
	'App.Notes.NotesService'
];

/**
 * @name NotesCreateController
 * @module App.Notes
 * @constructor NotesCreateController
 * @param $window {object} Window object
 * @param notesService {object} Notes servis for communication with apiary
 */
function NotesCreateController($window, notesService) {
	var vm = this;
	
	vm.createNote = createNote;
	
	vm.noteTitle = "";
	
	function createNote() {
		var promise;
		promise = notesService.noteCreate(null, vm.noteTitle);
		
		promise.then(function(response) {
			if (response.status === 201) {
				var data = response.data;
				console.log("Přidání poznámky s ID: " + data.id + " proběhlo v pořádku");
				// Does reload after update to refresh list of notes
				//$window.location.reload();
			}
		});
		promise.catch(function() {
			console.log("Přidání poznámky se nezdařilo");
		});
	}
}