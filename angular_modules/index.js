/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

/* Initialize templates module */
angular
	.module('templates', []);
	
angular
	.module('appEngine', [
		'ui.router',
		'App.Notes',
		'App.Lang',
		'templates',
		'pascalprecht.translate',
		'pasvaz.bindonce'
		]
	)
	
	// Set language constant for app 
	.constant('App.LANGUAGE_ID', (function (window) {
		var languageId = 'en';

		if (window.localStorage) {
			languageId = window.localStorage.getItem('selectedLanguage');
		}

		return languageId;
	})(window))
	
	// Config for lang translations
	.config([
		'$translateProvider', 
		'$httpProvider', 
		'App.LANGUAGE_ID',
		function langConfig($translateProvider, $httpProvider, languageId) {
			var defaultLanguage = 'cs';

			$translateProvider.fallbackLanguage('cs');
			$translateProvider.useSanitizeValueStrategy('escape');
			
			$translateProvider.useStaticFilesLoader({
				prefix: '/lang/locale-',
				suffix: '.json'
			});

			if(!languageId) {
				languageId = defaultLanguage;
				localStorage.setItem('selectedLanguage', defaultLanguage);
			}

			$translateProvider.preferredLanguage(languageId);
			
			// set accpet language header for all requests
			$httpProvider.defaults.headers.common['Accept-Language'] = languageId;
	}]);