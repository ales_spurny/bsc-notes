/**
 * Created by
 * Aleš Spurný <ales.spurny@bsc-ideas.com>
 * on 10.04.2016.
 */

'use strict';

angular
	.module('App.Lang')
	.controller('App.Lang.LangController', LangController);
	
LangController.$inject = [
	'$http',
	'$translate',
	'$scope'
];

/**
 * @name LangController
 * @module App.Lang
 * @constructor LangController
 * @param $http {object} Angular service for communication with the remote HTTP servers
 * @param $translate {object} Angular translate service object
 * @param $scope {object} Angular scope object
 */
function LangController($http, $translate, $scope) {
	$scope.lang = $translate.preferredLanguage();

	$scope.changeLanguage = function(langKey){
		$translate.use(langKey);

		localStorage.setItem('selectedLanguage', langKey);
		/** set accept language header for all requests **/
		$http.defaults.headers.common['Accept-Language'] = $translate.preferredLanguage();
	};
}